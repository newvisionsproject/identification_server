#!/bin/sh
# launcher.sh
# navigate to home directory, 
# then to python_apps, 
# then start identification_server, 
# then back home

cd /
cd home/pi/python_apps
sudo python3 identification_server.py
cd /
