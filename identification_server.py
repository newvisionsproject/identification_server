from twisted.web import server, resource
from twisted.internet import reactor, endpoints
import socket



class Counter(resource.Resource):
    isLeaf = True
    numberRequests = 0

    def render_GET(self, request):
        hostname = socket.gethostname()
        self.numberRequests += 1
        request.setHeader(b"content-type", b"text/plain")
        content = u"I am {0}. This is request #{1}\n".format(hostname, self.numberRequests)
        return content.encode("ascii")

endpoints.serverFromString(reactor, "tcp:31000").listen(server.Site(Counter()))
reactor.run()
